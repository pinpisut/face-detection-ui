import fetch from 'isomorphic-fetch';

require('es6-promise').polyfill()

const params = {
  headers: { 'Content-Type': 'application/json' }
}

const handleUrl = url => {
  if (/^http:\/\//.test(url)) {
    // ต่อ corsproxy    
    if (process.env.NODE_ENV === 'development' && !/^http:\/\/localhost/.test(url)) {
      return `http://${url.replace(/^http:\/\//g, '')}`
      // return `http://localhost:1337/${url.replace(/^http:\/\//g, '')}`
    }
    return url
  }
  return `${url}`
}

const fetchJson = url => {
  const xhttp = new XMLHttpRequest() //  eslint-disable-line
  return new Promise((resolve, reject) => {
    xhttp.onreadystatechange = function() {
      if (this.readyState === 4 && this.status === 200) {
        try {
          const data = xhttp.response
          resolve(JSON.parse(data))
        } catch (error) {
          reject(error)
        }
      }
    }
    xhttp.open('GET', handleUrl(url), true)
    xhttp.send()
  })
}

const getParams = (param = {}) => {
  const res = {
    method: param.method ? param.method : 'GET',
    headers: { 'Content-Type': 'application/json', ...param.headers }
  }
  if (res.method === 'POST') res.body = JSON.stringify(param.body ? param.body : {})
  return res
}

const fetchUrl = (url, param) => {
  return new Promise((resolve, reject) => {
    fetch(handleUrl(url), getParams(param))
      .then(res => res.json())
      .then(data => {
        try {
          resolve(data)
        } catch (error) {
          reject(error)
        }
      })
  })
}

export { fetchUrl }

export default fetchJson
