import React, { Component } from 'react';
import styled from 'styled-components';
import socketIOClient from 'socket.io-client';
import logo from './logo.svg';
import './App.css';
import { getData } from './actions/action';

const imgSource = require('./assets/test.png');

const HitMid = styled.div`
  width: 5px;
  height: 240px;
  background: #ff0000;
  position: absolute;
  top: ${props => (props.isClick ? '70px' : '175px')}
  left: 960px;
`;

const AreaCal = styled.div`
  width: 160px;
  height: 240px;
  background-color: rgba(255, 0, 0, 0.3);
  position: absolute;
  top: ${props => (props.isClick ? '70px' : '175px')}
  left: 880px;
`;

const TouchBtn = styled.div`
  display:flex;
  width: 250px;
  height: 200px;
  background-Color: #efefef;
  justify-content: center;
  align-items: center;
  margin-top: 10px;
`;

const Containner = styled.div`
  width: 1920px;
  height: 1080px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  background-color: #babab4;
`;

const ContainnerPicture = styled.div`
  width: 100%;
  height: 240px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 20px;
`;

const ContainnerResult = styled.div`
  width: 1000px;
  height: 240px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 20px;
  flex-direction: column;
  background-color: #efefef;
`;

let myVar;
const socket = socketIOClient('http://192.168.1.55:5000');


class App extends Component {

  state = {
    nowPeople: 0,
    allAge: 0,
    noAgeMore: 0,
    allGender: 'male',
    isClick: true,
    imageBase64: ""
  };

  handleData = () => {
    console.log('Click start data >>>>> ');
    this.setState({
      nowPeople: this.state.nowPeople + 1,
      allAge: 0,
      noAgeMore: 0,
      allGender: 'male',
      isClick: false
    });

    this.handleGetDataInterval();
  }

  componentDidMount = () => {
    socket.on('faceData', res => {
      this.handleCalculate(res);
    });
  }

  handleGetDataInterval = () => {
    
    socket.emit('requestFaceData', true); // change 'red' to this.state.color
    
    // myVar = setInterval(() => {
    //     getData().then(res => {
    //       console.log('res', res);
    //       const age = res.age;
    //       const gender = res.gender;
    //       const x = res.facex;
    //       const y = res.facey;
    //       const confi_age = res.confidentAge;
    //       const confi_gender = res.confidentGender;
    //       let significantAge = false;
    //       let significantGender = false;
    //       if (confi_age >= 400){
    //         significantAge = true;
    //       }
    //       if (confi_gender >= 200){
    //         significantGender = true;
    //       }

    //       if (x >= 400 && x <= 1200){ // (x >= 400 && x <= 1200)
    //         this.setState({
    //           allAge: significantAge? this.state.allAge + age : this.state.allAge,
    //           allGender: significantGender? gender : this.state.allGender,
    //           noAgeMore: significantAge? this.state.noAgeMore + 1 : this.state.noAgeMore,
    //           imageBase64: res.image
    //         });
    //       } else {
    //         console.log('clear interval at handleDataInterval >>> ');
    //         console.log('faceX >>> '+ res.facex);
    //         clearInterval(myVar);

    //         this.setState({
    //           isClick: true,
    //           imageBase64: ""
    //         })
    //       }

    //       return res;
    //     });
    //   }, 4000);
    // console.log('myVar 1', myVar);
  };

  handleCalculate = res => {
    console.log('handleCalculate >>> ', res);
    const { age, gender, faceX, faceY, confidentAge, confidentGender } = res;
    if (this.state.isClick === false){
      if (faceX > 0){
          let significantAge = false;
          let significantGender = false;
          if (confidentAge >= 400){
            significantAge = true;
          }
          if (confidentGender >= 200){
            significantGender = true;
          }
      
          if (faceX >= 400 && faceX <= 1200){ // (x >= 400 && x <= 1200)
            this.setState({
              allAge: significantAge? this.state.allAge + age : this.state.allAge,
              allGender: significantGender? gender : this.state.allGender,
              noAgeMore: significantAge? this.state.noAgeMore + 1 : this.state.noAgeMore,
              imageBase64: res.image
            });
          } else {
            console.log('clear interval at handleDataInterval >>> ');
            console.log('faceX >>> '+ faceX);
            // clearInterval(myVar);
            socket.emit('requestFaceData', false);
            
            this.setState({
              isClick: true,
              imageBase64: ""
            })
          }10
      } else {
        this.setState({
          isClick: true,
          imageBase64: ""
        });

        socket.emit('requestFaceData', false);
      }
    }
    
  }

  handleStopData = () => {
    console.log('Click stop data >>>> ');
    // console.log('myVar clear', myVar)
    // clearInterval(myVar);
    socket.emit('requestFaceData', false);
    

    this.setState({
      isClick: true,
      imageBase64: ""
    })

    /* console.log('all age >>> ', this.state.allAge);
    console.log('all gender >>> ', this.state.allGender);
    console.log('no age >>>>> ', this.state.noAgeMore);
    console.log('calculate age >>> ', this.state.allAge/this.state.noAgeMore); */

  }

  render() {
    const { nowPeople, allAge, noAgeMore, allGender, isClick, imageBase64 } = this.state;

    return (
      <Containner>
        {imageBase64 !== "" &&
          <ContainnerPicture>
            <img src={imageBase64} />
            <AreaCal isClick={isClick}/>
            <HitMid isClick={isClick}/>
          </ContainnerPicture>
        }
        {isClick &&
          <TouchBtn onClick={() => this.handleData()}><h1>TOUCH ME</h1></TouchBtn>
        }
        <TouchBtn onClick={() => this.handleStopData()}><h1>FORCE STOP</h1></TouchBtn>
        <ContainnerResult>
          <h1>now Person: {nowPeople}</h1>
          <h1>Age : {Math.floor(allAge/noAgeMore)}</h1>
          <h1>Gender : {allGender}</h1>
        </ContainnerResult>
      </Containner>
    );
  }
}

export default App;
